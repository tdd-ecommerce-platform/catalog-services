package com.tdd.catalog.services.impl;

import com.tdd.catalog.constant.Constant;
import com.tdd.catalog.constant.ProductTypeEnum;
import com.tdd.catalog.dto.BaseResponse;
import com.tdd.catalog.dto.ProductDto;
import com.tdd.catalog.entity.Category;
import com.tdd.catalog.entity.Product;
import com.tdd.catalog.mapper.ProductMapper;
import com.tdd.catalog.repository.CategoryRepository;
import com.tdd.catalog.repository.ProductRepository;
import com.tdd.catalog.services.ProductService;
import com.tdd.catalog.vm.product.CreateGeneralProductVM;
import com.tdd.catalog.vm.product.UpdateProductVM;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final CategoryRepository categoryRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResponse<ProductDto> createGeneralProduct(CreateGeneralProductVM request) {
        Product product = new Product();
        product.setProductType(ProductTypeEnum.valueOf(request.getProductType()));
        product.setName(request.getName());
        product.setDescription(request.getDescription());
        product.setRegularPrice(request.getDefaultPrice() == null ? new BigDecimal("0") : request.getDefaultPrice());
        product.setActiveStartDate(LocalDateTime.now());
        product.setSku(UUID.randomUUID().toString().toUpperCase());
        product.setAvailableOnline(Constant.STR_N);
        product.setIsOnline(Constant.STR_Y);
        product.setCurrency(Constant.PRODUCT.CURRENCY_CODE.VND);
        product.setUri(request.getUri());

        ConcurrentHashMap<String, String> a = new ConcurrentHashMap<>();

        productRepository.save(product);
        return BaseResponse.ok(productMapper.toDto(product));
    }

    @Override
    public BaseResponse<ProductDto> updateProduct(String productId, UpdateProductVM request) {
        return null;
    }
}
