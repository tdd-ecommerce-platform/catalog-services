package com.tdd.catalog.services;

import com.tdd.catalog.dto.BaseResponse;
import com.tdd.catalog.dto.ProductDto;
import com.tdd.catalog.vm.product.CreateGeneralProductVM;
import com.tdd.catalog.vm.product.UpdateProductVM;

public interface ProductService {
    BaseResponse<ProductDto> createGeneralProduct(CreateGeneralProductVM request);

    BaseResponse<ProductDto> updateProduct(String productId, UpdateProductVM request);
}
