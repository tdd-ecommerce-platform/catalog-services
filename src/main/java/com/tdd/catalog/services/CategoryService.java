package com.tdd.catalog.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tdd.catalog.dto.BaseResponse;
import com.tdd.catalog.dto.CategoryDto;
import com.tdd.catalog.dto.PagingDto;
import com.tdd.catalog.vm.DeleteEntityVM;
import com.tdd.catalog.vm.category.CreateCategoryVM;
import com.tdd.catalog.vm.category.UpdateGeneralCategoryVM;

import java.util.List;

public interface CategoryService {
    BaseResponse<CategoryDto> createCategory(CreateCategoryVM request) throws JsonProcessingException;

    BaseResponse<CategoryDto> updateGeneralCategory(UpdateGeneralCategoryVM request) throws JsonProcessingException;

    BaseResponse<PagingDto<CategoryDto>> paging(Integer page, Integer size);

    BaseResponse<List<CategoryDto>> findAll();

    BaseResponse<CategoryDto> findByUrl(String categoryUrl);

    BaseResponse<?> deleteCategories(DeleteEntityVM deleteVM);

    BaseResponse<CategoryDto> findCategoryDetail(String categoryId);
}
