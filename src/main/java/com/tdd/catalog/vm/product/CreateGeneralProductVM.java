package com.tdd.catalog.vm.product;

import com.tdd.catalog.vm.BaseRequest;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Set;

/*Tạo thông tin cơ bản cho tất cả các loại product*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateGeneralProductVM implements BaseRequest {

    @NotBlank(message = "Tên sản phẩm không được trống")
    @Size(max = 255, message = "Tên sản phẩm không quá 255 ký tự")
    private String name;

    @Size(max = Integer.MAX_VALUE /2, message = "Mô tả sản phẩm quá dài")
    private String description;

    @Pattern(regexp = "^(SIMPLE|GROUPED|EXTERNAL|VARIANT)$", message = "Loại sản phẩm không hợp lệ")
    private String productType;

    @DecimalMin(value = "0", message = "Default Price không được âm")
    @DecimalMax(value = "10000000000", message = "Default Price không được quá 10.000.000.000 đ")
    private BigDecimal defaultPrice;

    @Size(max = 500, message = "URI của sản phẩm không quá 800 ký tự")
    @NotBlank(message = "URI sản phẩm không được trống")
    private String uri;

    @NotNull(message = "Category sản phẩm không được trống")
    private Set<String> categoryIds;
}
