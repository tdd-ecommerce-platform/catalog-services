package com.tdd.catalog.vm.product;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class UpdateProductVM implements Serializable {
}
