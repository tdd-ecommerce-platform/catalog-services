package com.tdd.catalog.vm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteEntityVM implements BaseRequest {
    private Set<String> ids = new HashSet<>();
}
