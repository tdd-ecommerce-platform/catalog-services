package com.tdd.catalog.config;

import org.hibernate.search.backend.elasticsearch.analysis.ElasticsearchAnalysisConfigurationContext;
import org.hibernate.search.backend.elasticsearch.analysis.ElasticsearchAnalysisConfigurer;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

/**
 * @author thanglv on 26/03/2022
 * @project recruiter-api
 */
@Component
public class HibernateAnalysisConfigurer implements ElasticsearchAnalysisConfigurer {
    @Override
    public void configure(ElasticsearchAnalysisConfigurationContext context) {
        context.analyzer("ngram_analyzer")
                .custom()
                .tokenizer("standard")
                .charFilters("html_strip")
                .tokenFilters("asciifolding", "lowercase", "ngram");// ngram => cắt từ search : năng trẻ => n, ăn, g, t, rẻ

        context.analyzer("default_analyzer")
                .custom()
                .tokenizer("standard")
                .charFilters("html_strip")
                .tokenFilters("asciifolding", "lowercase");
    }
}