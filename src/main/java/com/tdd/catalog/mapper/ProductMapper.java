package com.tdd.catalog.mapper;

import com.tdd.catalog.dto.ProductDto;
import com.tdd.catalog.entity.Product;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductDto toDto(Product product);
}
