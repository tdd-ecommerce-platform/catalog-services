package com.tdd.catalog.exeception;

import com.tdd.catalog.utils.ResponseCode;

public class RequestValidationException extends RuntimeException {

    private ResponseCode responseCode;

    public RequestValidationException(ResponseCode responseCode, String msg) {
        super(msg);
        this.responseCode = responseCode;
    }

    public ResponseCode getResponseCode() {
        return responseCode;
    }
}
