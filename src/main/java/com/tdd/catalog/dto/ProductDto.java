package com.tdd.catalog.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tdd.catalog.constant.Constant;
import com.tdd.catalog.constant.ProductTypeEnum;
import com.tdd.catalog.entity.CategoryProduct;
import com.tdd.catalog.entity.DataDrivenEnum;
import com.tdd.catalog.entity.ProductAsset;
import com.tdd.catalog.entity.Variant;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

@Data
@NoArgsConstructor
public class ProductDto implements Serializable {
    private String productId;

    private LocalDateTime activeStartDate;

    private LocalDateTime activeEndDate;

    // Dynamic attribute là một phần của product, admin thêm thông tin phụ lưu trên sản phẩm
    // [{"name":"hotRange", "label":"Độ cay", "isArray": false, "value":"fuckk"}, {"name":"hotRange2", "label":"Độ cay", "isArray": true, "value":"['fuck', 'fuck2']"}]
    private Map<String, String> attributes;

    // Sản phẩm có sẵn cho cửa hàng trực tuyến hay không Y/N
    private String availableOnline;

    // Chi phí sản xuất hoặc chi phí nhập hàng, dùng để tính số tiền lãi
    private String cost;

    // Currency of all prices on the product (Kiểu product có giá bán lẻ , giá bán buôn, giá nhập -> dùng chung currency)
    private String currency;

    // Giá bán thường của sản phẩm
    private String regularPrice;

    // cho biết sản phẩm này đang được giảm giá và đang ở mức nào. Nếu salePrice < regularPrice thì sẽ lấy salePrice
    private String salePrice;

    // Mô tả sản phẩm, có thể chứa mã HTMl để display cho KH xem
    private String description;

    // Mô tả ngắn sản phẩm
    private String shortDescription;

    // Kích thước chiều sâu của sản phẩm 3 chiều, có liên hệ với dimension unit
    private String depth;

    // Kích thước chiều cao của sản phẩm 3 chiều
    private String height;

    // Kích thước chiều dài của sản phẩm
    private String width;

    // Đơn vị đo cho 3 loại kích thước trên
    private String dimensionUnits;

    // Cân nặng của sản phẩm
    private String weight;

    // đơn vị đo của
    private String weightUnits;

    // sản phẩm có được giảm giá hay không Y/N
    private String discountable;

    // Sản phẩm có thể nhận tại cửa hàng hay địa điểm thực tế thay vì vận chuyển hay không Y/N
    private String eligibleForPickup;

    //một json map data về mức giá tương ứng theo shipping method, kiểu ship standard 7 ngày thì rẻ hơn ship express nhanh 1 ngày
    private String fulfillmentFlatRates;

    // Trường này dành riêng cho bundled product, theo tìm hiểu qua loa thì nó lưu danh sách thông tin các product bundle item json
    private Map<String, String> includedProducts;

    /* Nếu product hoặc bất kỳ biến th của nó có thể được bán riêng lẻ trong cửa hàng
     * hoặc chúng có phải tách rời khỏi sản phẩm như một tiện ích bổ sung không
     * Thông thường nếu product not individually thì nó không được show ở search results
     * */
    private String individuallySold;

    // Xác định thời điểm cần kiểm tra còn hàng trong kho của product NEVER / ADD_TO_CART
    private String inventoryCheckStrategy;

    // Xác định thời điểm nào thì sản phẩm của KH đặt được giữ chỗ, để tránh người khác mua hết sản phẩm KH đặt NEVER, ADD_TO_CART, and SUBMIT_ORDER
    private String inventoryReservationStrategy;

    // Loại kho hàng là kho hàng ảo hay là kho thật PHYSICAL / VIRTUAL
    private String inventoryType;

    // <meta name="keywords" /> tag
    private String keywords;

    // Tóm lại nó cũng là một kiểu product mà nó có nhiều product con trong đó (Không phải là biến thể giống như variant product (SML red, white,..)).
    // Nó chỉ gọi là gom 2 hoặc nhiều sản phẩm có sẵn vào bán
    // Cột này đuợc đánh dấu có phải merchandising product hay không
    private String merchandisingProduct;

    // Khi product là Merchandising thì người dùng thêm sản phẩm vào giỏ hàng lần 1, nếu thêm vào giỏ hàng lần 2 thì nó display là 2 sản phẩm riêng biệt hay là một sản phẩm
    // COMBINE - gộp lại thành 1, SEPARATE  hiển thị thành 2 sản phẩm riêng biệt ở card, REJECT_OR_IGNORE - Báo lỗi khi KH cố add sản phẩm đã có sẵn trong card
    private String mergingType;

    // Hiển thị ở thẻ meta tag
    private String metaDescription;

    // hiển thị ở thẻ meta tag
    private String metaTitle;

    // Tên sản phẩm
    private String name;

    // sản phẩm mà ko online thì ko hiển thị ở store front và không tìm kiếm hoặc bán được Y/N
    private String isOnline;

    // string json quy định các options của sản phẩm như kiểu Size, Color, ....
    private Map<String, String> options;

    // đơn giản hoá việc ghi lại số lượng reviews của một sản phẩm, cột này ghi lại tổng số review của KH
    private String reviewsNumberOfReviews;

    // overall rating (điểm rating trung bình)
    private Double reviewsRating;

    /* đơn vị đo rating là start hay là thumbs */
    private String reviewsRatingUnits;

    /*Xác định xem sản phẩm có hiển thị trong kết quả search của KH hay không*/
    private String searchable;

    /* Là mã định danh sản phẩm, khi sản phẩm có nhiều biến thể như màu sắc, kích cỡ thì mỗi biến thể có sku khác nhau, check unique*/
    private String sku;

    /* Mã vạch của sản phẩm */
    private String upc;

    /*SEO URI thân thiện vs product thay vì productId, nó thường được dùng để generate theo kiểu
     * /category-uri/product-name ...
     * */
    private String uri;

    /* Mã số thuế của sản phẩm */
    private String taxCode;


    // Số lượng sản phẩm tối thiểu mà phải add vào cart
    private Integer minThreshold;

    // Số lượng sản phẩm tối đa được add vào cart
    private Integer maxThreshold;

    /*Loại sản phẩm, hiện tại đang hỗ trợ common 5 loại product type:
     * SIMPLE,  VARIANT, GROUPED, EXTERNAL
     *  */

    /*Tính năng này tạm thời chưa phát triển, người dùng có thể customize
     * lại product type của riêng mình, base từ 5 loại ở bên trên
     * */
    private String businessType;

    /* Một số các nhãn đơn giản để phân loại sản phẩm*/
    private String tags;

    /*Các bin thể của sản phẩm*/
    public Set<Variant> variants;

    public Set<ProductAsset> productAssets;

    /*Category của sản phẩm*/
//    private Set<CategoryProduct> productCategories;

    /*Sản phẩm bị xoá hay chưa*/
    private String isDeleted;

    //Số lượng mặt hng có sẵn
    private Integer quantityAvailable;
}
