package com.tdd.catalog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tdd.catalog.dto.BaseResponse;
import com.tdd.catalog.dto.ProductDto;
import com.tdd.catalog.services.ProductService;
import com.tdd.catalog.vm.product.CreateGeneralProductVM;
import com.tdd.catalog.vm.product.UpdateProductVM;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
@SecurityRequirement(name = "Authorization")
@Tag(name = "ProductController - Api liên quan tới catalog product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/general-product")
    public BaseResponse<ProductDto> createGeneralProduct(@RequestBody @Valid CreateGeneralProductVM request) throws JsonProcessingException {
        return productService.createGeneralProduct(request);
    }

    @PutMapping("/{productId}")
    public BaseResponse<ProductDto> updateProduct(@PathVariable @Valid String productId, @RequestBody @Valid UpdateProductVM request) {
        return productService.updateProduct(productId, request);
    }
}
