package com.tdd.catalog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tdd.catalog.dto.BaseResponse;
import com.tdd.catalog.dto.CategoryDto;
import com.tdd.catalog.dto.PagingDto;
import com.tdd.catalog.services.CategoryService;
import com.tdd.catalog.vm.DeleteEntityVM;
import com.tdd.catalog.vm.category.CreateCategoryVM;
import com.tdd.catalog.vm.category.UpdateGeneralCategoryVM;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
@SecurityRequirement(name = "Authorization")
@Tag(name = "CategoryController - Api liên quan tới catalog category")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping
    public BaseResponse<CategoryDto> createCategory(@RequestBody @Valid CreateCategoryVM request) throws JsonProcessingException {
        return categoryService.createCategory(request);
    }

    @PutMapping
    public BaseResponse<CategoryDto> updateCategory(@RequestBody @Valid UpdateGeneralCategoryVM request) throws JsonProcessingException {
        return categoryService.updateGeneralCategory(request);
    }

    @GetMapping("/paging")
    public BaseResponse<PagingDto<CategoryDto>> findPagingCategory(@RequestParam @Valid @NotNull Integer page, @RequestParam @Valid @NotNull Integer size) {
        return categoryService.paging(page, size);
    }

    @GetMapping("/all")
    public BaseResponse<List<CategoryDto>> findAllCategory() {
        return categoryService.findAll();
    }

    @GetMapping("/{categoryUrl}")
    public BaseResponse<CategoryDto> findByUrl(@PathVariable @Valid @NotNull String categoryUrl) {
        return categoryService.findByUrl(categoryUrl);
    }

    @DeleteMapping
    public BaseResponse<?> deleteCategory(@RequestBody @Valid @NotNull DeleteEntityVM deleteVM) {
        return categoryService.deleteCategories(deleteVM);
    }

    @GetMapping("/detail/{categoryId}")
    public BaseResponse<CategoryDto> findCategoryDetail(@PathVariable @Valid @NotNull String categoryId) {
        return categoryService.findCategoryDetail(categoryId);
    }
}
